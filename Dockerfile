FROM alpine:3.11
MAINTAINER Ganex <suporte@ganex.com.br>

# Install dependecies
RUN set -ex \
    && apk update -qq \
    && apk add --update ca-certificates curl bash python py-pip zip

# Install aws-cli
RUN set -ex \
    && pip install --upgrade awscli

# Install Nodejs/serverless
RUN set -ex \
    && apk add --update nodejs npm git \
    && npm install --silent --progress=false -g serverless@2.72.3 \
    && npm install -D serverless-plugin-typescript typescript serverless-webpack serverless-domain-manager

CMD ["bash"]